import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  registerMode = false;
  values: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    // this.getValues();
  }

  registerToggle() {
    this.registerMode = true;
  }

  /*
  getValues() {

    // HttpInterceptor serait à explorer pour ce genre de traitement.
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));

    this.http.get('http://localhost:5000/api/values', {headers}).subscribe(response => {
      this.values = response;
    }, error => {
      console.log(error);
    });
  }
  */

  cancelRegisterMode(registerMode: boolean) {
    this.registerMode = registerMode;
  }

}
